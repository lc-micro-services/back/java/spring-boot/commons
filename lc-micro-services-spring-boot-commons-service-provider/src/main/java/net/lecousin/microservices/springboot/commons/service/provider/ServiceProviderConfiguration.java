package net.lecousin.microservices.springboot.commons.service.provider;

import java.security.GeneralSecurityException;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity.CsrfSpec;
import org.springframework.security.config.web.server.ServerHttpSecurity.FormLoginSpec;
import org.springframework.security.config.web.server.ServerHttpSecurity.HttpBasicSpec;
import org.springframework.security.config.web.server.ServerHttpSecurity.LogoutSpec;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.context.NoOpServerSecurityContextRepository;
import org.springframework.web.server.session.WebSessionManager;

import net.lecousin.microservices.springboot.commons.CommonsConfiguration;
import net.lecousin.microservices.springboot.commons.CommonsHttpConfiguration;
import net.lecousin.microservices.springboot.commons.http.HttpFilter;
import net.lecousin.microservices.springboot.commons.security.InternalJwtAuthenticationManager;
import net.lecousin.microservices.springboot.commons.security.JwtFilter;
import net.lecousin.microservices.springboot.commons.security.LcRoleHierarchy;
import net.lecousin.microservices.springboot.commons.security.Permission;
import net.lecousin.microservices.springboot.commons.traceability.TraceabilityService;
import reactor.core.publisher.Mono;

@Configuration
@Import({CommonsConfiguration.class, CommonsHttpConfiguration.class})
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
public class ServiceProviderConfiguration {

	@Bean
	RoleHierarchy roleHierarchy(List<Permission> permissions, DefaultMethodSecurityExpressionHandler handler) {
		if (permissions.isEmpty()) throw new IllegalStateException("Permissions of your service provider must be configured as @Bean");
		RoleHierarchy h = new LcRoleHierarchy(permissions);
		handler.setRoleHierarchy(h);
		return h;
	}
	
	@Bean
	HttpFilter lcHttpFilter(TraceabilityService traceabilityService) {
		return new HttpFilter(traceabilityService);
	}
	
	@Bean
	InternalJwtAuthenticationManager authenticationManager() throws GeneralSecurityException {
		return new InternalJwtAuthenticationManager();
	}
	
	@Bean
	WebSessionManager webSessionManager() {
		return exchange -> Mono.empty();
	}
	
	@Bean
	SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http, ReactiveAuthenticationManager authManager) {
		return http
		.csrf(CsrfSpec::disable)
		.formLogin(FormLoginSpec::disable)
		.httpBasic(HttpBasicSpec::disable)
		.logout(LogoutSpec::disable)
		.authorizeExchange(auth -> auth
			.pathMatchers(HttpMethod.GET, "/actuator/**").permitAll()
			.pathMatchers("/**").authenticated()
		)
		.addFilterBefore(new JwtFilter(authManager), SecurityWebFiltersOrder.HTTP_BASIC)
		.securityContextRepository(NoOpServerSecurityContextRepository.getInstance())
		.build();
	}
}
