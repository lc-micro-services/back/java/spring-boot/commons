package net.lecousin.microservices.springboot.commons.traceability;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TraceabilityService {

	public void addStep(Traceability trace, Step step) {
		// TODO
		log.debug(trace + " : " + step);
	}
	
}
