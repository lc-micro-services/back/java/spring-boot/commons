package net.lecousin.microservices.springboot.commons.security;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public class LcRoleHierarchy implements RoleHierarchy {

	private Map<String, Set<SimpleGrantedAuthority>> includes = new HashMap<>();
	
	public LcRoleHierarchy(List<Permission> permissions) {
		Set<SimpleGrantedAuthority> all = new HashSet<>();
		permissions.forEach(p -> {
			var set = p.allIncluded().stream().map(Permission::toAuthority).map(SimpleGrantedAuthority::new).collect(Collectors.toSet());
			includes.put(p.toAuthority(), set);
			all.addAll(set);
		});
		includes.put(Permission.ROOT, all);
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getReachableGrantedAuthorities(Collection<? extends GrantedAuthority> authorities) {
		Set<GrantedAuthority> result = new HashSet<>();
		authorities.forEach(a -> {
			result.add(a);
			Set<SimpleGrantedAuthority> included = includes.get(a.getAuthority());
			if (included != null)
				result.addAll(included);
		});
		return result;
	}
	
}
