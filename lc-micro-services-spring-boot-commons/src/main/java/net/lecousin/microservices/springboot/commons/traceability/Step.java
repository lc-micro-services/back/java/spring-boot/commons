package net.lecousin.microservices.springboot.commons.traceability;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Step {
	private String app;
	private String service;
	private String details;
	private Type type;
	private long timestamp;
	private int resultCode;
	private String error;
	
	public enum Type {
		METHOD_CALL_START,
		METHOD_CALL_END,
		REST_CALL_RECEIVED,
		REST_CALL_RESPONSE_SENT,
		REST_CALL_SENT,
		REST_CALL_RESPONSE_RECEIVED,
		MESSAGE_RECEIVED,
		TASK_STARTED
	}
	
}
