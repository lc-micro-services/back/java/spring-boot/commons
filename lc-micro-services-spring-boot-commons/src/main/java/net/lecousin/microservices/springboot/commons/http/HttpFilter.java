package net.lecousin.microservices.springboot.commons.http;

import java.util.Locale;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;

import lombok.RequiredArgsConstructor;
import net.lecousin.microservices.springboot.commons.traceability.Step;
import net.lecousin.microservices.springboot.commons.traceability.Traceability;
import net.lecousin.microservices.springboot.commons.traceability.TraceabilityService;
import reactor.core.publisher.Mono;
import reactor.util.context.ContextView;

@RequiredArgsConstructor
public class HttpFilter implements WebFilter {
	
	private static final String EXCHANGE_ATTRIBUTE_START_CONTEXT = "lc-start-context";

	private final TraceabilityService traceabilityService;
	
	@Value("${spring.application.name}")
	private String appName;
	
	@Override
	public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
		exchange.getResponse().beforeCommit(() -> Mono.deferContextual(ctx -> {
			Traceability.fromContext(ctx).or(() ->
				Optional.ofNullable((ContextView) exchange.getAttributes().get(EXCHANGE_ATTRIBUTE_START_CONTEXT))
				.flatMap(Traceability::fromContext)
			).ifPresent(trace -> traceabilityService.addStep(trace, Step.builder()
				.app(appName)
				.timestamp(System.currentTimeMillis())
				.type(Step.Type.REST_CALL_RESPONSE_SENT)
				.details(exchange.getRequest().getMethod().name() + ' ' + exchange.getRequest().getPath())
				.build()
			));
			return Mono.empty();
		}));
		return Mono.defer(() -> {
			long time = System.currentTimeMillis();
			return chain.filter(exchange)
			.contextWrite(ctx -> {
				var trace = Traceability.fromHeaders(exchange.getRequest().getHeaders()).orElseGet(Traceability::create);
				traceabilityService.addStep(trace, Step.builder()
					.app(appName)
					.timestamp(time)
					.type(Step.Type.REST_CALL_RECEIVED)
					.details(exchange.getRequest().getMethod().name() + ' ' + exchange.getRequest().getPath())
					.build()
				);
				ctx = trace.toContext(ctx);
				ctx = ctx.put(Locale.class, exchange.getLocaleContext().getLocale());
				exchange.getAttributes().put(EXCHANGE_ATTRIBUTE_START_CONTEXT, ctx);
				return ctx;
			});
		});
	}
	
}
