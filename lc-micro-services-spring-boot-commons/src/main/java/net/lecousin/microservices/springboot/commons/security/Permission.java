package net.lecousin.microservices.springboot.commons.security;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Permission {
	
	public static final String ROOT = "ROOT";

	private String service;
	private String right;
	private List<Permission> includes = new LinkedList<>();
	
	public Set<Permission> allIncluded() {
		Set<Permission> set = new HashSet<>();
		set.add(this);
		includes.forEach(p -> set.addAll(p.allIncluded()));
		return set;
	}
	
	public String toAuthority() {
		return "permission:" + service + ":" + right;
	}
	
}
