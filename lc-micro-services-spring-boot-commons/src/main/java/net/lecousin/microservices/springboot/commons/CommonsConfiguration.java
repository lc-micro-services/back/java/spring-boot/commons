package net.lecousin.microservices.springboot.commons;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import net.lecousin.microservices.springboot.commons.cache.CacheExpirationService;

@Configuration
public class CommonsConfiguration {

	@Bean
	CacheExpirationService cacheExpirationService() {
		return new CacheExpirationService();
	}
	
}
