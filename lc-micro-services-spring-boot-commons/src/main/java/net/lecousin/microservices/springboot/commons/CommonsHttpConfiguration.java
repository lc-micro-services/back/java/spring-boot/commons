package net.lecousin.microservices.springboot.commons;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import net.lecousin.microservices.springboot.commons.http.PageRequestMethodArgumentResolver;
import net.lecousin.microservices.springboot.commons.traceability.TraceabilityService;

@Configuration
@Import({WebFluxConfiguration.class})
public class CommonsHttpConfiguration {

	@Bean
	TraceabilityService traceabilityService() {
		return new TraceabilityService();
	}
	
	@Bean
	PageRequestMethodArgumentResolver pageRequestMethodArgumentResolver() {
		return new PageRequestMethodArgumentResolver();
	}

}
