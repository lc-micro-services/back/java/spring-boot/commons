package net.lecousin.microservices.springboot.commons.traceability;

import java.security.SecureRandom;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import reactor.util.context.Context;
import reactor.util.context.ContextView;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Traceability {

	private String correlationId;
	private String tenantId;
	private String username;
	
	private static final String CONTEXT_KEY = "lc-tracability";
	
	public Context toContext(Context ctx) {
		return ctx.put(CONTEXT_KEY, this);
	}
	
	public static Optional<Traceability> fromContext(ContextView ctx) {
		return ctx.getOrEmpty(CONTEXT_KEY);
	}
	
	
	private static final int CORRELATION_ID_SIZE = 12;
	private static final SecureRandom RANDOM = new SecureRandom();
	
	public static Traceability create() {
		Traceability trace = new Traceability();
		byte[] random = new byte[CORRELATION_ID_SIZE];
		RANDOM.nextBytes(random);
		trace.setCorrelationId(Base64.getUrlEncoder().encodeToString(random));
		return trace;
	}
	
	private static final String HEADER_TRACE_ID = "X-LC-TRACE-ID";
	private static final String HEADER_TRACE_WHO = "X-LC-TRACE-WHO";

	public static Optional<Traceability> fromHeaders(Map<String, List<String>> headers) {
		if (!headers.containsKey(HEADER_TRACE_ID))
			return Optional.empty();
		Traceability trace = new Traceability();
		trace.setCorrelationId(headers.get(HEADER_TRACE_ID).get(0));
		List<String> list = headers.get(HEADER_TRACE_WHO);
		if (list != null && !list.isEmpty()) {
			String who = list.get(0);
			int i = who.indexOf(';');
			if (i < 0) {
				trace.setTenantId(who);
			} else {
				trace.setTenantId(who.substring(0, i));
				trace.setUsername(who.substring(i + 1));
			}
		}
		return Optional.of(trace);
	}
	
	public void toHeaders(Map<String, List<String>> headers) {
		headers.put(HEADER_TRACE_ID, List.of(correlationId));
		if (tenantId != null) {
			StringBuilder s = new StringBuilder(tenantId);
			if (username != null)
				s.append(';').append(username);
			headers.put(HEADER_TRACE_WHO, List.of(s.toString()));
		}
	}
}
