package net.lecousin.microservices.springboot.commons.client;

import java.util.Locale;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.ExchangeFunction;

import lombok.RequiredArgsConstructor;
import net.lecousin.microservices.springboot.commons.traceability.Step;
import net.lecousin.microservices.springboot.commons.traceability.Traceability;
import net.lecousin.microservices.springboot.commons.traceability.TraceabilityService;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class InternalCallFilter implements ExchangeFilterFunction {

	private final TraceabilityService traceabilityService;
	
	@Value("${spring.application.name}")
	private String appName;
	
	@Override
	public Mono<ClientResponse> filter(ClientRequest request, ExchangeFunction next) {
		return Mono.deferContextual(ctx -> {
			var trace = Traceability.fromContext(ctx).orElseGet(Traceability::create);
			traceabilityService.addStep(trace, Step.builder()
				.app(appName)
				.timestamp(System.currentTimeMillis())
				.type(Step.Type.REST_CALL_SENT)
				.details(request.url().toString())
				.build()
			);
			ClientRequest.Builder newRequest = ClientRequest.from(request)
				.headers(headers -> trace.toHeaders(headers));
			Locale locale = ctx.getOrDefault(Locale.class, Locale.ENGLISH);
			newRequest.header(HttpHeaders.ACCEPT_LANGUAGE, locale.toString());
			return ctx.<Mono<SecurityContext>>getOrEmpty(SecurityContext.class).orElse(Mono.<SecurityContext>empty())
				.map(sec -> Optional.ofNullable(sec.getAuthentication()))
				.switchIfEmpty(Mono.just(Optional.empty()))
				.flatMap(authOpt -> {
					if (authOpt.isPresent()) {
						var auth = authOpt.get();
						Object cred = auth.getCredentials();
						if (cred instanceof String token)
							newRequest.header(HttpHeaders.AUTHORIZATION, "Bearer " + token);
					}
					return next.exchange(newRequest.build())
						.doOnSuccess(response -> responseReceived(trace, request, response, null))
						.doOnError(error -> responseReceived(trace, request, null, error));
				});
		});
	}
	
	private void responseReceived(Traceability trace, ClientRequest request, ClientResponse response, Throwable error) {
		traceabilityService.addStep(trace, Step.builder()
			.app(appName)
			.timestamp(System.currentTimeMillis())
			.type(Step.Type.REST_CALL_RESPONSE_RECEIVED)
			.details(request.url().toString())
			.resultCode(response != null ? response.statusCode().value() : 0)
			.error(error != null ? error.toString() : null)
			.build()
		);
	}
	
}
