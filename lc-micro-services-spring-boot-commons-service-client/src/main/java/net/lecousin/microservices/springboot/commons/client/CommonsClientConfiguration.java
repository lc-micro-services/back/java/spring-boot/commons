package net.lecousin.microservices.springboot.commons.client;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.reactive.function.client.WebClient;

import net.lecousin.microservices.springboot.commons.CommonsConfiguration;
import net.lecousin.microservices.springboot.commons.CommonsHttpConfiguration;
import net.lecousin.microservices.springboot.commons.traceability.TraceabilityService;

@Configuration
@Import({CommonsConfiguration.class, CommonsHttpConfiguration.class, LcMicroServicesClientFactory.class})
@ComponentScan(basePackages = "net.lecousin.microservices.client")
public class CommonsClientConfiguration {

	@Bean
	@LoadBalanced
	WebClient.Builder loadBalancedWebClientBuilder() {
		return WebClient.builder();
	}
	
	@Bean
	InternalCallFilter internalCallFilter(TraceabilityService traceabilityService) {
		return new InternalCallFilter(traceabilityService);
	}
	
}
