package net.lecousin.microservices.springboot.commons.client;

import java.io.IOException;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.SimpleMetadataReaderFactory;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LcMicroServicesClientFactory implements ImportBeanDefinitionRegistrar, BeanFactoryAware {

	@Setter
	private BeanFactory beanFactory;
	
	@Override
	public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry, BeanNameGenerator importBeanNameGenerator) {
		log.info("Looking for @LcMicroServicesClient interfaces...");
		long startTime = System.currentTimeMillis();
		PathMatchingResourcePatternResolver resourceResolver = new PathMatchingResourcePatternResolver();
		Resource[] classResources;
		try {
			classResources = resourceResolver.getResources(ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + "/net/lecousin/microservices/client/**/*.class");
		} catch (IOException e) {
			log.error("Cannot analyze classpath", e);
			return;
		}
		SimpleMetadataReaderFactory metadataReaderFactory = new SimpleMetadataReaderFactory();
		String annotationName = LcMicroServicesClient.class.getName();
		int count = 0;
		for (Resource classResource : classResources) {
			try {
				MetadataReader metadataReader = metadataReaderFactory.getMetadataReader(classResource);
				AnnotationMetadata annotationMetadata = metadataReader.getAnnotationMetadata();
				if (annotationMetadata.hasAnnotation(annotationName)) {
					Class<?> clazz = Class.forName(annotationMetadata.getClassName());
					try {
						LcMicroServicesClient annotation = clazz.getAnnotation(LcMicroServicesClient.class);
						registerClient(registry, clazz, annotation);
						count++;
					} catch (Exception e) {
						log.error("Error registering HttpExchange client {}", clazz.getName(), e);
					}
				}
			} catch (@SuppressWarnings("java:S1181") Throwable t) {
				// ignore
			}
		}
		log.info("{} @LcMicroServicesClient registered in {} ms.", count, System.currentTimeMillis() - startTime);
	}
	
	private void registerClient(BeanDefinitionRegistry registry, Class<?> clazz, LcMicroServicesClient annotation) {
		var builder = BeanDefinitionBuilder.genericBeanDefinition(LcMicroServicesClientFactoryBean.class)
		.addPropertyValue("clazz", clazz)
		.addPropertyValue("serviceName", annotation.service())
		.setLazyInit(true);
		registry.registerBeanDefinition(annotation.qualifier(), builder.getBeanDefinition());
	}
	
}
