package net.lecousin.microservices.springboot.commons.client;

import org.springframework.beans.factory.SmartFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.support.WebClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

import lombok.Setter;
import net.lecousin.microservices.springboot.commons.http.PageRequestHttpServiceArgumentResolver;

public class LcMicroServicesClientFactoryBean implements SmartFactoryBean<Object>, ApplicationContextAware {

	@Setter
	private ApplicationContext applicationContext;
	
	@Setter
	private Class<?> clazz;
	@Setter
	private String serviceName;
	
	@Override
	public boolean isEagerInit() {
		return false;
	}
	
	@Override
	public boolean isSingleton() {
		return false;
	}
	
	@Override
	public boolean isPrototype() {
		return true;
	}
	
	@Override
	public Object getObject() {
		String resolvedName = applicationContext.getEnvironment().resolvePlaceholders(serviceName);
		InternalCallFilter filter = applicationContext.getBean(InternalCallFilter.class);
		WebClient client = applicationContext.getBean(WebClient.Builder.class)
			.baseUrl("http://" + resolvedName)
			.filter(filter)
			.build();
		return HttpServiceProxyFactory.builderFor(WebClientAdapter.create(client))
			.customArgumentResolver(new PageRequestHttpServiceArgumentResolver())
			.build().createClient(clazz);
	}
	
	@Override
	public Class<?> getObjectType() {
		return clazz;
	}
	
}
