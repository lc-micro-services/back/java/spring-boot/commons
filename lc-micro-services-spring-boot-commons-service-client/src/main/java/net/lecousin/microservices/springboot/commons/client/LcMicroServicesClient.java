package net.lecousin.microservices.springboot.commons.client;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface LcMicroServicesClient {

	String service();
	
	String qualifier();
	
}
