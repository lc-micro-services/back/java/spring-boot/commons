package net.lecousin.microservices.springboot.commons.connector;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import net.lecousin.microservices.springboot.commons.CommonsConfiguration;

@Configuration
@Import(CommonsConfiguration.class)
public class CommonsConnectorConfiguration {

	@Bean
	ConnectorService connectorService() {
		return new ConnectorService();
	}
	
}
