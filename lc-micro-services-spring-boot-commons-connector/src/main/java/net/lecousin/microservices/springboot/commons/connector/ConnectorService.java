package net.lecousin.microservices.springboot.commons.connector;

import java.time.Duration;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.lecousin.microservices.springboot.commons.cache.CacheExpirationService;
import reactor.core.publisher.Mono;

@Service
public class ConnectorService implements ApplicationContextAware {

	@Autowired
	@Lazy
	private List<ConnectorFactory<?, ?>> factories;
	@Autowired
	private CacheExpirationService cache;
	@Value("${lc-micro-services.connectors.cache-expiration:#{null}}")
	private Map<String, Duration> expirationByType;
	@Value("${lc-micro-services.connectors.cache-expiration.default:15m}")
	private Duration defaultExpiration;
	
	@Setter
	private ApplicationContext applicationContext;
	
	private static final String CACHE_KEY = "ConnectorService.connectors";
	
	@SuppressWarnings("unchecked")
	public <C extends Connector> Mono<C> getConnector(Class<C> type, String implementation, String key, Map<String, Object> properties) {
		return Mono.defer(() -> {
			ConnectorFactory<C, ?> factory;
			if (implementation != null && !implementation.isBlank()) {
				var optFactory = factories.stream().filter(f -> type.isAssignableFrom(f.getConnectorClass()) && f.getImplementation().equals(implementation)).findAny();
				if (optFactory.isEmpty())
					return Mono.error(new IllegalArgumentException("No connector " + type.getName() + " found for implementation " + implementation));
				factory = (ConnectorFactory<C, ?>) optFactory.get();
			} else {
				var eligibles = factories.stream().filter(f -> type.isAssignableFrom(f.getConnectorClass())).toList();
				if (eligibles.size() == 1)
					factory = (ConnectorFactory<C, ?>) eligibles.get(0);
				else
					return Mono.error(new IllegalArgumentException("Expected a single connector type " + type.getName() + " but found is: " + eligibles.size()));
			}
			CacheKey ck = new CacheKey(factory.getType(), implementation, key);
			Duration expiration = expirationByType != null ? expirationByType.get(factory.getType()) : null;
			if (expiration == null)
				expiration = defaultExpiration;
			return cache.get(CACHE_KEY, ck, () -> createConnector(factory, key, properties), expiration, this::destroyConnector);
		});
	}
	
	private <C extends Connector, P> Mono<C> createConnector(ConnectorFactory<C, P> factory, String key, Map<String, Object> properties) {
		P props = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).convertValue(properties, factory.getPropertiesClass());
		return factory.create(props)
			.map(connector -> {
				applicationContext.getAutowireCapableBeanFactory().initializeBean(connector, "lc-micro-services-connector-" + factory.getType() + "-" + factory.getType() + "-" + key);
				return connector;
			});
	}
	
	private <C extends Connector> Mono<Void> destroyConnector(C connector) {
		return Mono.defer(() -> {
			applicationContext.getAutowireCapableBeanFactory().destroyBean(connector);
			return connector.destroy();
		});
	}
	
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	private static final class CacheKey {
		private String type;
		private String implementation;
		private String key;
	}
	
}
