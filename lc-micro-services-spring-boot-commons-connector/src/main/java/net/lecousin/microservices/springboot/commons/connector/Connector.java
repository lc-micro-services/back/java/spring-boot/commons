package net.lecousin.microservices.springboot.commons.connector;

import reactor.core.publisher.Mono;

public interface Connector {

	Mono<Void> destroy();
	
}
