package net.lecousin.microservices.springboot.commons.test;

import java.util.Properties;

import org.apache.commons.lang3.mutable.MutableObject;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.util.PropertyPlaceholderHelper;
import org.testcontainers.containers.GenericContainer;

import lombok.extern.slf4j.Slf4j;
import net.lecousin.microservices.commons.manifest.ConnectorManifest;

@Slf4j
public class ConnectorsInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

	static class Container extends GenericContainer<Container> {
		Container(String imageName) {
			super(imageName);
		}
		
		@Override
		public void close() {
			log.info("Shutting down docker container with image " + getDockerImageName());
			super.stop();
		}
	}
	
	@Override
	public void initialize(ConfigurableApplicationContext applicationContext) {
		var connectors = ConnectorManifest.load();
		MutableObject<TestPropertyValues> values = new MutableObject<>(TestPropertyValues.empty());
		connectors.forEach(connector -> {
			connector.getDependencies().forEach((name, dependency) -> {
				log.info("Starting docker container with image " + dependency.getImage() + ":" + dependency.getVersion());
				Container container = new Container(dependency.getImage() + ":" + dependency.getVersion());
				applicationContext.getBeanFactory().registerSingleton("testcontainer-" + dependency.getImage() + "-" + dependency.getVersion(), container);
				if (dependency.getExposedPorts() != null)
					dependency.getExposedPorts().forEach(port -> container.addExposedPort(port));
				if (dependency.getTestEnvironment() != null)
					for (var env : dependency.getTestEnvironment().entrySet())
						container.addEnv(env.getKey(), env.getValue());
				container.start();
				if (dependency.getTestProperties() != null) {
					PropertyPlaceholderHelper ph = new PropertyPlaceholderHelper("{{", "}}");
					Properties props = new Properties();
					props.setProperty("host", container.getHost());
					if (dependency.getExposedPorts() != null)
						dependency.getExposedPorts().forEach(port -> props.setProperty("port." + port, Integer.toString(container.getMappedPort(port))));
					dependency.getTestProperties().forEach((propName, propValue) -> {
						String value = ph.replacePlaceholders(propValue, props);
						values.setValue(values.getValue().and(propName + "=" + value));
						log.info("Set property: " + propName + "=" + value);
					});
				}
			});
		});
		values.getValue().applyTo(applicationContext);
	}
	
}
