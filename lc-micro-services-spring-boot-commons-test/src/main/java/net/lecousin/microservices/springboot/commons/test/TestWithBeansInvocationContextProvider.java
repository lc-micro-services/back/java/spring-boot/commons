package net.lecousin.microservices.springboot.commons.test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.extension.Extension;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.junit.jupiter.api.extension.TestTemplateInvocationContext;
import org.junit.jupiter.api.extension.TestTemplateInvocationContextProvider;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SuppressWarnings("rawtypes")
public class TestWithBeansInvocationContextProvider implements TestTemplateInvocationContextProvider {

	@Override
	public boolean supportsTestTemplate(ExtensionContext context) {
		return context.getRequiredTestClass().getAnnotationsByType(TestWithBeans.class).length > 0;
	}
	
	@Override
	public Stream<TestTemplateInvocationContext> provideTestTemplateInvocationContexts(ExtensionContext context) {
		TestWithBeans[] beansTypesAnnotations = context.getRequiredTestClass().getAnnotationsByType(TestWithBeans.class);
		List<Pair<Class, String>> firstBeanTestCases = createBeanInvocations(beansTypesAnnotations[0]);
		List<List<Pair<Class, String>>> invocations = new LinkedList<>();
		for (var firstBeanTestCase : firstBeanTestCases) invocations.add(List.of(firstBeanTestCase));
		for (int i = 1; i < beansTypesAnnotations.length; ++i)
			invocations = combine(invocations, createBeanInvocations(beansTypesAnnotations[i]));
		return invocations.stream().map(this::contextWithBeans);
	}
	
	private List<Pair<Class, String>> createBeanInvocations(TestWithBeans bean) {
		return Arrays.stream(bean.qualifiers()).map(qualifier -> Pair.of((Class) bean.value(), qualifier)).toList();
	}
	
	private List<List<Pair<Class, String>>> combine(List<List<Pair<Class, String>>> list, List<Pair<Class, String>> add) {
		List<List<Pair<Class, String>>> result = new LinkedList<>();
		for (var currentTestCase : list) {
			for (var newTestCase : add) {
				List<Pair<Class, String>> combine = new LinkedList<>(currentTestCase);
				combine.add(newTestCase);
				result.add(combine);
			}
		}
		return result;
	}
	
	private TestTemplateInvocationContext contextWithBeans(List<Pair<Class, String>> beans) {
		return new TestTemplateInvocationContext() {
			@Override
			public String getDisplayName(int invocationIndex) {
				StringBuilder s = new StringBuilder("with");
				for (var bean : beans) {
					s.append(" bean ").append(bean.getKey().getSimpleName()).append('=').append(bean.getValue());
				}
				return s.toString();
			}
			
			@Override
			public List<Extension> getAdditionalExtensions() {
				Map<Class, String> beansMap = new HashMap<>();
				for (var bean : beans)
					beansMap.put(bean.getKey(), bean.getValue());
				return List.of(
					new ParameterResolver() {
						@Override
						public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) {
							return beansMap.containsKey(parameterContext.getParameter().getType());
						}
						
						@SuppressWarnings("unchecked")
						@Override
						public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
							Class beanType = parameterContext.getParameter().getType();
							String beanName = beansMap.get(beanType);
							return SpringExtension.getApplicationContext(extensionContext).getBean(beanName, beanType);
						}
					}
				);
			}
		};
	}
	
}
