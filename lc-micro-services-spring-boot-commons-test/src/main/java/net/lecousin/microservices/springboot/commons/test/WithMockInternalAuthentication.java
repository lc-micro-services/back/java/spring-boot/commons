package net.lecousin.microservices.springboot.commons.test;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.springframework.security.test.context.support.WithSecurityContext;

@Retention(RetentionPolicy.RUNTIME)
@WithSecurityContext(factory = WithMockInternalAuthenticationSecurityContextFactory.class)
public @interface WithMockInternalAuthentication {

	String username() default "user";
	String[] permissions() default {};
	
}
