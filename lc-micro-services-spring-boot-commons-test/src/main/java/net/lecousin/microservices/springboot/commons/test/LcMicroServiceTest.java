package net.lecousin.microservices.springboot.commons.test;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextCustomizerFactories;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration(
	classes = LcMicroServiceTestConfiguration.class,
	initializers = ConnectorsInitializer.class
)
@ContextCustomizerFactories(factories = LcMicroServiceTestContextCustomizerFactory.class) 
public @interface LcMicroServiceTest {

	String service();
	
}
