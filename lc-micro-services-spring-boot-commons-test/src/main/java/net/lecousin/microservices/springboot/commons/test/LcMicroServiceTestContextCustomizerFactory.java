package net.lecousin.microservices.springboot.commons.test;

import java.util.List;

import org.springframework.test.context.ContextConfigurationAttributes;
import org.springframework.test.context.ContextCustomizer;
import org.springframework.test.context.ContextCustomizerFactory;

public class LcMicroServiceTestContextCustomizerFactory implements ContextCustomizerFactory {

	@Override
	public ContextCustomizer createContextCustomizer(Class<?> testClass, List<ContextConfigurationAttributes> configAttributes) {
		LcMicroServiceTest annotation = testClass.getAnnotation(LcMicroServiceTest.class);
		if (annotation == null) return null;
		return new LcMicroServiceTestContextCustomizer(annotation.service());
	}
	
}
